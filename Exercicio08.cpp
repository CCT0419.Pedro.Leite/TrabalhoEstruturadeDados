#include <iostream>

using namespace std;
int vetor[20];
const int tamanho=20;

void inserirValores(){
    for (int i=0;i<tamanho;i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> vetor[i];
    }
}
void imprimirVetor(){
    cout<<"\nVetor ordenado:\n";
    for (int i=0; i < tamanho; i++){
        cout << "Posicao "<< (i+1) <<": "<<vetor[i]<<endl;
    }
}

void selectionSort(){
    for (int i=0; i < tamanho; i++){
        int menor = i;
        for (int j=i+1; j < tamanho; j++){
            if (vetor[j] < vetor[menor]){
                menor = j;
            }
        }
        if (i != menor){
            int temp = vetor[i];
            vetor[i] = vetor[menor];
            vetor[menor] = temp;
        }
    }
}


int main()
{
    inserirValores();
    selectionSort();
    imprimirVetor();
}
