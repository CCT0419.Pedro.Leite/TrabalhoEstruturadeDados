#include <iostream>

#include <iostream>

using namespace std;

const int tamanho =10;
int* vetor[tamanho];

void inicializaVetor(){
cout << "Inicializando o vetor " << endl;
for (int i=0; i < tamanho; i++){
vetor[i]=new int;
*(vetor[i])=0;
    }
}

void lerVetor(){
cout << "Leia os valores do vetor" << endl;
for (int i=0; i < tamanho; i++){
cout << "Posicao "<<(i+1)<<": ";
cin >> *(vetor[i]);
    }
}

void imprimirVetores(){
cout << "Imprima os valores do vetor "<< endl;
for (int i=0; i < tamanho; i++){
cout << "Posicao "<<(i+1)<<": "<<*(vetor[i])<<endl;
    }
}

int main()
{
inicializaVetor();
lerVetor();
imprimirVetores();
cout<<endl<<"Fim do programa!!";

}

