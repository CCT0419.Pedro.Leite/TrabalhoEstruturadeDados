#include <iostream>

using namespace std;
const int tamanho=10;
int vetor[tamanho],x;


void inserirValores(){
    for (int i=0;i<tamanho;i++){
        cout << "Posicao "<<(i+1)<<": ";
        cin >> vetor[i];
    }
}
void bubbleSort(){
    for (int i=0; i < tamanho; i++){
        for (int j= tamanho-1; j>=i; j--){
            if (vetor[j-1]>vetor[j]){
                int temp = vetor[j-1];
                vetor[j-1]=vetor[j];
                vetor[j]=temp;
            }
        }
    }
}
void insertionSort(){
    for (int i=1;i<tamanho;i++){
        int comp = vetor[i];
        int j = i - 1;
        for (;j>=0 && comp < vetor[j];j--){
            vetor[j+1]=vetor[j];
        }
        vetor[j+1] = comp;
    }
}
void selectionSort(){
    for (int i=0; i < tamanho; i++){
        int menor = i;
        for (int j=i+1; j < tamanho; j++){
            if (vetor[j] < vetor[menor]){
                menor = j;
            }
        }
        if (i != menor){
            int temp = vetor[i];
            vetor[i] = vetor[menor];
            vetor[menor] = temp;
        }
    }
}
void escolha(){
    cout<<"Qual forma de ordenacao?(1 - bubble sort, 2 - insertion sort, 3 - selection sort)";
    cin>>x;
        if(x==1){
        bubbleSort();
}
        if(x==2){
        insertionSort();
    }
        if(x==3){
        selectionSort();
    }
}
void imprimirVetor(){
    cout<<"\nVetor ordenado:\n";
    for (int i=0; i < tamanho; i++){
        cout << "Posicao "<< (i+1) <<": "<<vetor[i]<<endl;
    }
}

int main()
{
    inserirValores();
    escolha();
    imprimirVetor();
}
